DEBUG_BUILD := testing

OBJ := netpp.o
TARGET := netpp

HARDENING ?= 1
OPTIMIZE ?= -Os

LIBS   := -lrt -lssl -lz

CFLAGS := -Wall -Wextra -pipe -Wwrite-strings -Wsign-compare \
					-Wshadow -Wformat=2 -Wundef -Wstrict-prototypes   \
					-fstrict-aliasing -fno-common -Wformat-security \
					-Wformat-y2k -Winit-self -Wredundant-decls \
					-Wstrict-aliasing=3 -Wswitch-default -Wswitch-enum \
					-Wno-system-headers -Wundef -Wvolatile-register-var \
					-Wcast-align -Wbad-function-cast -Wwrite-strings \
					-Wold-style-definition  -Wdeclaration-after-statement \
					-fstack-protector -fstrict-overflow -Wstrict-overflow=5

CFLAGS += -ggdb3 # -Werror

CFLAGS += -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE

ifeq ($(HARDENING),1)
ifeq ($(OPTIMIZE),)
OPTIMIZE := -O2
endif
endif

ifeq ($(HARDENING),1)
CFLAGS += -D_FORTIFY_SOURCE=2 -fstack-protector-all -fPIE
LDFLAGS += -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -pie
endif


ifdef EPOLL
	EXTRA_CFLAGS := -DHAVE_EPOLL
endif

ifdef DEBUG_BUILD
	#EXTRA_CFLAGS += -DDEBUG
endif

.SUFFIXES:
.SUFFIXES: .c .o

all: $(TARGET)

%.o : %.c
	$(CC) -c $(CFLAGS) $(EXTRA_CFLAGS) $(CPPFLAGS) $< -o $@

$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(EXTRA_CFLAGS) $(LIBS) -o $(TARGET) $(OBJ)

clean:
	-rm -f $(OBJ) $(TARGET) core

cscope:
	cscope -R -b

